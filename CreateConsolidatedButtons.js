/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * Creates the buttons that will create different transactions for the lines
 * on the Consolidated Transaction
 */

define(['N/record','N/search','N/runtime','/SuiteScripts/Cumula 3/SearchForConsolidateLines','SuiteScripts/Cumula 3/Search-20'],
    function (record,search,runtime,lineItems,searchScript)
    {
        function onBeforeLoad(context)
        {
        	try{   
	        	var transaction = context.newRecord;
	        	var id = transaction.getValue('id');
	            log.debug('onBeforeLoad','Id - ' + id);

	        	if (context.type != context.UserEventType.VIEW){
					return;
				}
	        	
	        	context.form.clientScriptModulePath = 'SuiteScripts/Cumula 3/CreateConsolidatedButtons-Client.js';
	        	var pdf = transaction.getValue('custrecord_proforma_inv');
	        	var invPDF = transaction.getValue('custrecord_ct_pdf');
	        	var shipNotice = transaction.getValue('custrecord_shipping_notice');
	        	
	        	if(isNull(pdf) && isNull(shipNotice)){
		        	context.form.addButton({
	                    id : 'custpage_create_shipNotice',
	                    label : 'Create Ship Notice',
	                    functionName : 'createTransactions('+ id +')'
	                });
	        	};
	        	if(isNull(invPDF) && !isNull(pdf) && !isNull(shipNotice)){
	        		var date = transaction.getValue('custrecord_ct_invoice_date');
	        		var ctNum = transaction.getValue('custrecord_ct_id');
	                context.form.addButton({
	                    id : 'custpage_create_invoice',
	                    label : 'Create Invoice',
	                    functionName : 'createTransactions('+ id +',"invoice","'+ date +'","'+ ctNum +'")'
	                });
	        	}
	        	if(showBillBtn(id)){
	                context.form.addButton({
	                    id : 'custpage_create_bill',
	                    label : 'Create Bill',
	                    functionName : 'createTransactions('+ id +',"bill")'
	                });
	        	}
                context.form.addButton({
                    id : 'custpage_delete',
                    label : 'Delete',
                    functionName : 'deleteTransactions('+ id +')'
                });
                /*var userObj = runtime.getCurrentUser();
				var userId = userObj.id;
                if(userId == '7'){
                	context.form.addButton({
                        id : 'custpage_test',
                        label : 'Test Ship',
                        functionName : 'createTransactions('+ id +')'
                    });
                }*/
	        	
        	}catch(e){
        		log.error('onBeforeSubmit','Error - ' + e.message);
        	}
           
        }
        
        /**
         * Gets the amount of Consolidated Transaction Line Item records and compares to the
         * amount of lines with the Consolidated Transactions on it. Returns true if the amounts
         * match
         * 
         * @ctId
         */
        function showBillBtn(ctId){
	       	 var consolidatedLines = lineItems.SearchLines(ctId);
	       	 
	       	 var filters = [
	               search.createFilter({
	            	   name: 'custbody_consolidated_trans',
	            	   operator : search.Operator.ANYOF,
	            	   values: ctId
	               }),
	               search.createFilter({
	            	   name: 'mainline',
	            	   operator : search.Operator.IS,
	            	   values: false
	               }),
	             ];
	
	        	 var newSearch = searchScript.init('vendorbill',null,filters);
	            var searchResults = newSearch.getResults();
	            log.debug('getBills','Results - ' + JSON.stringify(searchResults));
	            
	            if(consolidatedLines.length == searchResults.length){
	            	return false;
	            }
	            
	            return true;
         }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
        
        return {
        	beforeLoad: onBeforeLoad
        };
    });

