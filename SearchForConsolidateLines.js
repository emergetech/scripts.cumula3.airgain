/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 * 
 * Searches for the 'Consolidated Transaction Line Item' records by the id of the
 * 'Consolidated Transaction Header' field and returns the records.
 * 
 */
define(['N/record','N/search','/SuiteScripts/Cumula 3/Search-20'],function(record,search,searchScript){
	
	function SearchLines(ctId,mainFilter){
		try{ 
			var filters = [
               search.createFilter({
            	   name: 'custrecord_ct_header',
            	   operator : search.Operator.ANYOF,
            	   values: ctId
               })
           ];
           
			var secondaryFilter;
		   if(mainFilter == null || mainFilter == 'po') {
			   mainFilter = 'custrecord_ct_po_id';
			   secondaryFilter = 'custrecord_ct_so_id';
		   }else{
			   mainFilter = 'custrecord_ct_so_id'
			   secondaryFilter = 'custrecord_ct_po_id'
		   }
           var columns = [
              search.createColumn({
            	  name: mainFilter,
        		  sort: search.Sort.ASC
              }),
              search.createColumn({
            	  name: 'custrecord_ct_line_unique_key'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_line_id'
              }),
              search.createColumn({
            	  name: secondaryFilter
              }),
              search.createColumn({
            	  name: 'custrecord_ct_item'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_qty'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_rate'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_amount'
              }),
              search.createColumn({
            	  name: 'custrecord_ct__desc'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_line_poline'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_line_cust_ord_num'
              }),
              search.createColumn({
            	  name: 'custrecord_ct_line_part_no'
              }),
              search.createColumn({
             	  name: 'custrecord_so_line_unique_key'
               })
           ];
           
           var newSearch = searchScript.init('customrecord_ct_line_item',null,filters,columns);
           var searchResults = newSearch.getResults();
           log.debug('SearchLines','Consolidated Line Results - ' + JSON.stringify(searchResults));
           return searchResults;
		}catch(e){
			log.error('SearchLines','Error - ' + e.message);
		}
	}
	
	return{
		SearchLines: SearchLines
	}
});