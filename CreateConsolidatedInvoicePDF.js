/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * 
 * 
 */
define(['N/record','N/xml','N/render','N/search','/SuiteScripts/Cumula 3/Search-20','N/email','N/file','/SuiteScripts/Cumula 3/SearchForConsolidateLines','N/redirect','N/runtime'],
    function (record,xmlModule,render,search,searchScript,email,file,lineItems,redirect,runtime)
    {
		var PDF_FOLDER = '601';
		var CT_NAME = '';
        function run(context)
        {
            try{
                var headerId = context.request.parameters.transactionId;
                var type = context.request.parameters.type;
                var createCTInv = context.request.parameters.createCTInv;
                var createShipPDF = context.request.parameters.createShipPDF;
                log.debug('run','Header Id - ' + headerId + ' Type - ' + type);           

                var lines = lineItems.SearchLines(headerId);
                var ct = record.load({
                    type: 'customrecord_consolidated_transaction',
                    id: headerId,
                    isDynamic: true
                });
                var customer = record.load({
                    type: record.Type.CUSTOMER,
                    id: ct.getValue('custrecord_ct_customer'),
                    isDynamic: true
                });


                log.debug('run','createCTInv - ' + createCTInv + ' createShipPDF = ' + createShipPDF);
                if(type == 'shipping' || createShipPDF == "true"){
                	var templatesInfo = [{'id':'CUSTTMPL_CONSOLIDATED_PACKING_LIST','name':'Shipping Notice - ' + ct.getValue('name'),'fieldId':'custrecord_shipping_notice'},
                	                   {'id':'CUSTTMPL_PROFORMA_INVOICE','name':'Proforma Invoice -' + ct.getValue('name'),'fieldId':'custrecord_proforma_inv'},
                	                   {'id':'CUSTTMPL_CONTRACT_TO_SELL','name':'Contract to Sell -' + ct.getValue('name'),'fieldId':'custrecord_contract_to_sell'}];

    				for(var i = 0; i < templatesInfo.length; i++){
    					var renderer = render.create(); 
    	                renderer.setTemplateByScriptId({
    	                    scriptId: templatesInfo[i].id
    	                  });
    	                var myContent = renderer.addRecord({
    	                	templateName: 'record', 
    	                	record: ct
    	                });
    	                renderer.addRecord({
    	                	templateName: 'customer', 
    	                	record: customer
    	                });
    	                renderer.addSearchResults({
    	                		templateName: 'lines', 
    	                		searchResult: lines
    	                });
    	                try{
//    	                	var recordRendered = xmlModule.escape({
//    	                		xmlText : renderer.renderAsPdf() });
    		                var recordRendered = renderer.renderAsPdf();		                
    		                recordRendered.folder = PDF_FOLDER;
    		                recordRendered.name = templatesInfo[i].name;
    		                var pdfId = recordRendered.save();
                          log.debug('run','PDF Id - ' + pdfId);
    		                ct.setValue(templatesInfo[i].fieldId,pdfId);
//    		                context.response.writeFile(recordRendered,true);
//    		                return;
    	                }catch(e){
    	                	log.error('run','Error = ' + e);
    	                }
    				}
                }
                //else{
                if(type == 'consolidated' || createCTInv == "true"){
                	var renderer = render.create(); 
	                renderer.setTemplateByScriptId({
	                    scriptId: 'CUSTTMPL_AIRGAIN_CONSOLIDATED_INV'
	                  });
	                //CUSTTMPL_CONSOLIDATED_TRANSACTION_PDF
	                var myContent = renderer.addRecord({
	                	templateName: 'record', 
	                	record: ct
	                });
	                renderer.addRecord({
	                	templateName: 'customer', 
	                	record: customer
	                });
	                renderer.addSearchResults({
	                		templateName: 'lines', 
	                		searchResult: lines
	                });
	                // Loop through the search results and sum the amounts returned
                    var ctTotal = 0;
                    for(var l = 0; l < lines.length; l++){
                    	ctTotal = ctTotal + parseFloat(lines[l].getValue('custrecord_ct_amount'));
                    }
                  	log.debug('run','CT Total - ' + ctTotal);
                 	renderer.addCustomDataSource({ 
                    format: render.DataSource.OBJECT, 
                    alias: "JSON",
					data: {"recAmount":formatCurrency(ctTotal)}
					});
	                try{
		                var recordRendered = renderer.renderAsPdf();
		                //context.response.writeFile(recordRendered,true);
		                recordRendered.folder = PDF_FOLDER;
		                recordRendered.name = 'Consolidated Invoice -'+ ct.getValue('name');
		                var pdfId = recordRendered.save();
		                ct.setValue('custrecord_ct_pdf',pdfId)
	                }catch(e){
	                	log.error('run','Error = ' + e);
	                }
                }

                ct.save();
                //context.response.writeFile(pdfFile,true);
            	redirect.toRecord({
	    			type : 'customrecord_consolidated_transaction',
	    			id : headerId,
	    			isEditMode: false
            	});
            }catch(e){
                log.error("run", "Error - " + e);
            }
            
           
        }

        
        /**
         * Formats the value sent in as a currency value
         */
        function formatCurrency(n){
            return "$" + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        }
        
        return {
            onRequest: run,
            
        };
    });