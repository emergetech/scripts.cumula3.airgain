/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * Loads the Adresses from the customer record into a custom field on the form.
 * 
 * AIRGAIN-24
 */

define(['N/record','N/search','/SuiteScripts/Cumula 3/Search-20','N/ui/serverWidget','N/runtime'],
    function (record,search,searchScript,serverWidget,runtime)
    {
        function onBeforeLoad(context)
        {
        	try{
        		if (context.type === context.UserEventType.CREATE || runtime.executionContext != 'USERINTERFACE'){
        			return;
        		}
	        	var rec = context.newRecord;
	        	var id = rec.getValue('id');
	        	var customer = rec.getValue('custrecord_ct_customer');
	        	if(isNull(customer)){
	        		return;
	        	}
	        	
	        	var results = getCustomerAddresses(customer);
				
				var form = context.form;
				var addr = form.addField({
        		    id : 'custpage_ship_to_options',
        		    type : serverWidget.FieldType.SELECT,
        		    label : 'Ship To Select'
        		});
				//Adds the addresses to the field
				addr.addSelectOption({
        		    value : '',
        		    text : '',
        		    isSelected: false
        		});
				
				for(var i = 0; i < results.length;i++){
//            		log.debug('onBeforeLoad','Form - ' + JSON.stringify(groups[i]));
					addr.addSelectOption({
            		    value : results[i].getValue('address'),
            		    text : results[i].getValue('address'),
            		    isSelected: false
            		});
        		}
				
				form.insertField({ 
					field : addr,
					nextfield : 'custrecord_ship_to_addr' 
				});

	        	
        	}catch(e){
        		log.error('onAfterSubmit','Error - ' + e.message);
        	}
           
        }
        
        /**
         * Searches for the addresses on the customer record sent in
         * 
         * @param customer
         */
        function getCustomerAddresses(customer){
        	var filters = [
        	   search.createFilter({
        		   name: 'internalid',
        		   operator: search.Operator.ANYOF,
        		   values: customer
        	   })            
        	]
        	var columns = [
        	   search.createColumn({
        		   name: 'address'
        	   })                          
        	];
        	var newSearch = searchScript.init('customer',null,filters,columns);
			var results = newSearch.getResults();
			log.debug('onBeforeLoad',JSON.stringify(results));	
			
			return results;
        }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
        
        return {
        	beforeLoad: onBeforeLoad
        };
    });

