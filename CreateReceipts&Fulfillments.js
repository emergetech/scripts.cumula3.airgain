/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 * @NScriptType MapReduceScript
 * 
 * Creates the view that allows the users to choose what lines will be used to create the Consolidated Transaction.
 * 
 * AIRGAIN-5
 */
define(['N/search', 'N/record', '/SuiteScripts/Cumula 3/Search-20', 'N/runtime', '/SuiteScripts/Cumula 3/SearchForConsolidateLines'],
    function(search, record, searchScript, runtime, lineItems)
    { 	

        function getInputData()
        {
            try{
           
            	var scriptObj = runtime.getCurrentScript();
            	var ids = scriptObj.getParameter({name: 'custscript_consolidated_ids'});
            	log.debug('getInputData','Ids - ' + ids);
            	
            	ids = ids.split(',');            	
            	idsArr = [];
            	for(var i = 0; i < ids.length; i++){
            		//log.debug('getInputData','Ids Data - ' + ids[i]);
            		idsArr.push(ids[i].replace(/[^\w\s]/gi, ''));
            	}
            	
            	return idsArr;
            	
            }catch(e){
                log.error('getInputData','Error - ' + e.message);
            }

        }

        function map(context)
        {
        	try{
                var searchResult = JSON.parse(context.value);
                log.debug('map','Search Result - ' + JSON.stringify(searchResult));
                var tranTypes = ['po',null]
                log.debug('map','tranTypes - ' + tranTypes);
                for(var l = 0; l < tranTypes.length; l++){
                	var tranType = tranTypes[l];
                	log.debug('map','Tran Type - ' + tranType);
	                var searchResults = lineItems.SearchLines(searchResult,tranType);
	
	                var prevTransaction = '';
	                var poArr = [];
	                var data = [];
	                               
	                for(var i = 0; i < searchResults.length; i++){
	                	var transaction;
	                	if(tranType == 'po'){
	                		transaction = searchResults[i].getValue('custrecord_ct_po_id');
	                	}else{
	                		transaction = searchResults[i].getValue('custrecord_ct_so_id');
	                	}
	                	//var po = searchResults[i].getValue('custrecord_ct_po_id');
	                	log.debug('map','Transaction - ' + transaction + ' Tran Type - ' + tranType);
	                	
	                	if(transaction != prevTransaction && prevTransaction != ''){
	                		if(tranType == 'po'){
	                			createReceipt(poArr,searchResult);
	                		}else{
	                			//createFulfillment(searchResults[i - 1],searchResult)
	                			createFulfillment(poArr,searchResult)
	                		}
	                		
	                		poArr = [];
	                	}
	                	var obj = {};
	                	obj.po = transaction;
	                	obj.poLineId = searchResults[i].getValue('custrecord_ct_line_id');
	                	obj.poUniqueKey = searchResults[i].getValue('custrecord_ct_line_unique_key');
	                	obj.poQty = searchResults[i].getValue('custrecord_ct_qty');
	                	obj.poItem = searchResults[i].getValue('custrecord_ct_item');
	                	obj.poRate = searchResults[i].getValue('custrecord_ct_rate');
	                	obj.soUniqueKey = searchResults[i].getValue('custrecord_so_line_unique_key');
	                	poArr.push(obj);
	                	
	                	if(i == searchResults.length - 1){
	                		if(tranType == 'po'){
	                			createReceipt(poArr,searchResult);
	                		}else{
	                			//createFulfillment(searchResults[i],searchResult)
	                			createFulfillment(poArr,searchResult)
	                		}
	                	}
	                	prevTransaction = transaction;
	                }
                }
        	}catch(e){
        		log.error('map','Error - ' + e.message);
        	}
        }
        
        function createReceipt(data,ctId){
        	try{
	        	log.debug('createReceipt','Data - ' + JSON.stringify(data));
	        	
	        	//addTransactionToPO(data,ctId) 
	        	
	        	var receipt = record.transform({
					fromType: record.Type.PURCHASE_ORDER, 
					fromId: data[0].po,
					toType: record.Type.ITEM_RECEIPT, 
					isDynamic: false
				});
	        	receipt.setValue('custbody_consolidated_trans',ctId);
	        	for(var i = 0; i < receipt.getLineCount('item'); i++){
	        		var orderLine = receipt.getSublistValue('item','orderline',i);
	        		log.debug('createReceipt','Order Line - ' + orderLine);
	        		receipt.setSublistValue('item','itemreceive',i,false);
	        		for(var j = 0; j < data.length; j++){
	        			log.debug('createReceipt','Data Loop - ' + JSON.stringify(data[j]));
	        			if(orderLine == data[j].poLineId){
	        				receipt.setSublistValue('item','itemreceive',i,true);
	        				receipt.setSublistValue('item','quantity',i,data[j].poQty);
	        				//receipt.setSublistValue('item','custcol_consolidated_transaction',i,ctId);
	        			}
	        		}
	        	}
	        	
	        	receipt.save();
        	}catch(e){
        		log.error('createReceipt','Error - ' + e.message);
        	}
        }
        
        /**
         * Adds the Consolidated Transaction to the Purchase Order lines that were
         * consolidated
         * 
         * @param data
         * @param ctId
         */
        function addTransactionToPO(data,ctId){
        	//log.debug('addTransactionToPO','Array - ' + data + ' PO Id - ' + poId);

        	var po = record.load({
        		type: record.Type.PURCHASE_ORDER,
        		id: data[0].po,
        		isDynamic: true
        	});

        	for(var i = 0; i < po.getLineCount('item'); i++){
        		var lineKey = po.getSublistValue('item','lineuniquekey',i);
        		log.debug('addTransactionToPO','PO Line Key - ' + lineKey);
        		for(var k = 0; k < data.length; k++){
        			if(data[k].poUniqueKey == lineKey){
        				var origQty = po.getSublistValue('item','quantity',i);
        				if(origQty != data[k].poQty){
        	        		/*po.setSublistValue('item','quantity',i,data[k].poQty);
        	        		po.setSublistValue('item','custcol_consolidated_transaction',i,ctId);*/
        					po.selectLine({
        						sublistId: 'item',
        						line: i 
        					});
        					po.setCurrentSublistValue('item','quantity',data[k].poQty);
        					po.commitLine({
        						sublistId: 'item' 
        					});
        				}
        			}
        		}
        	}
        	
        	po.save();
        }
        
        /**
         * Creates an Item Fulfillment by transforming the Sales Order record.
         * 
         * @param data
         * @param ctId
         */
        function createFulfillment(data,ctId){
        	try{
	        	log.debug('createFulfillment','Data - ' + JSON.stringify(data));
	        	var fulfillment = record.transform({
					fromType: record.Type.SALES_ORDER, 
					fromId: data[0].po,
					toType: record.Type.ITEM_FULFILLMENT, 
					isDynamic: false
				});
	        	fulfillment.setValue('shipstatus','C');
	        	fulfillment.setValue('custbody_consolidated_trans',ctId);
	        	var lineCnt = fulfillment.getLineCount('item');
	        	for(var i = 0; i < lineCnt;i++){
	        		var itemFound = false;
	        		var item = fulfillment.getSublistValue('item','item',i);
	        		var qty = fulfillment.getSublistValue('item','quantity',i);
	        		var lineKey = fulfillment.getSublistValue('item','custcol_so_line_unique_key',i);
	        		log.debug('createFulfillment','Fulfillment Line Key - ' + lineKey + ' Line Receive - ' + fulfillment.getSublistValue('item','itemreceive',i));
//	        		log.debug('createFulfillment','Fulfillment Item - ' + item + ' Fulfillment Qty - ' + qty);
	        	//	log.debug('createFulfillment','Data Length - ' + data.length );
	        		if(data.length != undefined){
		        		for(var k = 0; k < data.length; k++){
		        			log.debug('createFulfillment','Fulfillment Item - ' + item + ' Fulfillment Qty - ' + qty);
		        			//if(lineKey == data[k].getValue('custrecord_so_line_unique_key')){
		        			if(lineKey == data[k].soUniqueKey){
	        				//if(item == data[k].getValue('custrecord_ct_item') && qty == data[k].getValue('custrecord_ct_qty')){
		        				fulfillment.setSublistValue('item','itemreceive',i,true);
		        				fulfillment.setSublistValue('item','quantity',i,data[k].poQty);
	        					itemFound = true;
	        					break;
	        				}
	        			}
	        		}else{
	        			if(lineKey == data[k].soUniqueKey){
	        			//log.debug('createFulfillment','Data Item - ' + data.getValue('custrecord_ct_item') + ' Consolidated Qty - ' + data.getValue('custrecord_ct_qty') + ' Line Key - ' + data.getValue('custrecord_so_line_unique_key'));
	        			//if(item == data.getValue('custrecord_ct_item') && qty == data.getValue('custrecord_ct_qty')){
	        				fulfillment.setSublistValue('item','itemreceive',i,true);
	        				fulfillment.setSublistValue('item','quantity',i,data.poQty);
	        				itemFound = true;
	        			}
	        		}
	        		
	        		if(!itemFound){
	        			fulfillment.setSublistValue('item','itemreceive',i,false);
	        		}
	        	}
	     
        		
        		fulfillment.save();
        	}catch(e){
        		log.error('createFulfillment','Error - ' + e.message);
        	}
        }
        
		/******************************************************************
		 * If the value sent in is not null or an empty string, return true. 
		 *
		 * @param value
		 ******************************************************************/
    	function hasValue(value){
    		if(value != null && value != '' && value != undefined){
    			return true;
    		}
    		
    		return false;
    	}
       
        return {
            getInputData: getInputData,
            map: map
            //reduce: reduce,
//            summarize: summarize
        };
    });