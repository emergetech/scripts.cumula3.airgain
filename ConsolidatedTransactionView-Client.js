/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 * @NScriptType ClientScript
 * 
 */

define(['N/record','N/runtime','N/url'],
    function (rec,runtime,url)
    {
		function fieldChanged(context){
			try{
				var record = context.currentRecord;
				if(context.fieldId == 'custpage_line_selected'){
					var isSelected = record.getCurrentSublistValue('custpage_po_lines','custpage_line_selected');
					var currIndex = record.getCurrentSublistIndex({
						sublistId: 'custpage_po_lines' });
					var lineKey = record.getSublistValue('custpage_po_lines','custpage_po_linekey', currIndex);
					if(!isSelected){
						searchExistingLines(record,lineKey);
					}
				}
				
				if(context.fieldId == 'custpage_qty'){
					var qty = record.getCurrentSublistValue('custpage_po_lines','custpage_qty');
					var currIndex = record.getCurrentSublistIndex({
						sublistId: 'custpage_po_lines' });
					var maxQty = record.getSublistValue('custpage_po_lines','custpage_max_qty', currIndex);
					if(qty > maxQty){
						alert('The Max QTY you can consolidate for this line is ' + maxQty);
						record.setCurrentSublistValue('custpage_po_lines','custpage_qty',maxQty);
					}else if(qty < 0){
						alert('QTY has to be a positive number');
						record.setCurrentSublistValue('custpage_po_lines','custpage_qty',maxQty);
					}else{
						var lineKey = record.getSublistValue('custpage_po_lines','custpage_po_linekey', currIndex);
						//searchExistingLines(record,lineKey,qty);
						getChangedQtys(record,lineKey,qty);
					}
					
				}
				

				jQuery( "#custpage_po_linesunmarkall" ).click(function() {
					record.setValue('custpage_selected_lines','');
				});
				return true;


			}catch(e){
				alert('Error - ' + e.message);
			}
		}
		
		function searchExistingLines(record,lineKey,qty){
			try{
				var lineObj = record.getValue('custpage_selected_lines');
				if(lineObj == null || lineObj == ''){
					return true;
				}
				lineObj = JSON.parse(lineObj);
				//[{"cust":"36","vendor":"9","date":"2019-01-31T05:00:00.000Z","poNum":"265","soNum":"264","item":"10","qty":100,"rate":0.5,"lineId":"2","lineKey":"442"}]
				for(var i = 0; i < lineObj.length; i++){
					var objLineKey = lineObj[i].lineKey;
					//alert('Obj Line Key - ' + objLineKey + ' Line Key - ' + lineKey);
					if(objLineKey == lineKey){
						if(qty == null){
							lineObj.splice(i, 1);
						}else{
							lineObj[i].qty = qty;
						}
//						alert('Removing line');
						
					}
				}
				var selectedLinesValue = JSON.stringify(lineObj);
				record.setValue('custpage_selected_lines',selectedLinesValue);
//				alert(record.getValue('custpage_selected_lines'));
			}catch(e){
				alert('Error search - ' + e.message);
			}
		}
		
		function getChangedQtys(record,lineKey,qty){
			try{
				var qtyLines = record.getValue('custpage_changed_qtys');
				var parsedQtyLines = [];
				if(!isNull(qtyLines)){
					parsedQtyLines = JSON.parse(qtyLines);
				}else{
					insertIntoArr(record,lineKey,qty,parsedQtyLines);
					return;
				}
				
				var found = false;
				for(var i = 0; i < parsedQtyLines.length; i++){
					var objLineKey = parsedQtyLines[i].lineKey;
					if(objLineKey == lineKey){				
						//parsedQtyLines[i].qty = qty;
						parsedQtyLines.splice(i, 1);
						break;
					}
				}
				
//				if(!found && !isNull(qtyLines)){
					insertIntoArr(record,lineKey,qty,parsedQtyLines);
//				}
			}catch(e){
				alert('getSelectedLines Error - ' + e.message);
			}
		}
		
		function insertIntoArr(record,lineKey,qty,parsedQtyLines){
			var obj = {};
			obj.lineKey = lineKey;
			obj.qty = qty;
			parsedQtyLines.push(obj);				
			record.setValue('custpage_changed_qtys',JSON.stringify(parsedQtyLines));
		}
		
		function getSelectedLines(record){
			try{
				var lines = [];
				var lineKeys = [];
				var origLines = record.getValue('custpage_selected_lines');
//				alert(origLines);
				var parsedOrigLines;
				if(!isNull(origLines)){
					parsedOrigLines = JSON.parse(origLines);
				}
//				alert(parsedOrigLines);
				for(var i = 0; i < record.getLineCount('custpage_po_lines'); i++){
					var addLine = true;
					var selected = record.getSublistValue('custpage_po_lines','custpage_line_selected',i);
					if(selected){
						var lineKey = record.getSublistValue('custpage_po_lines','custpage_po_linekey',i);
						if(!isNull(origLines)){
							for(var b = 0; b < parsedOrigLines.length; b++){
								//alert('getSelectedLines: Line Key = ' + lineKey + ' Obj Line Key = ' + parsedOrigLines[b].lineKey);
								if(lineKey == parsedOrigLines[b].lineKey){
									addLine = false;
									break;
								}
							}
						}
						if(!addLine){
							continue;
						}
						//lines.push(lineKey);

						var obj = {};
						obj.customer = record.getSublistValue('custpage_po_lines','custpage_po_cust', i);
						obj.vendor = record.getSublistValue('custpage_po_lines','custpage_po_vendor', i);
						obj.date = record.getSublistValue('custpage_po_lines','custpage_po_date', i);
						obj.po = record.getSublistValue('custpage_po_lines','custpage_po_number', i);
						obj.soId = record.getSublistValue('custpage_po_lines','custpage_so_num',i);
						obj.item = record.getSublistValue('custpage_po_lines','custpage_item', i);
						obj.qty = record.getSublistValue('custpage_po_lines','custpage_qty', i);
						obj.rate = record.getSublistValue('custpage_po_lines','custpage_rate', i);
						obj.orderRefNum = record.getSublistValue('custpage_po_lines','custpage_order_reference', i);
						obj.lineId = record.getSublistValue('custpage_po_lines','custpage_po_lineid', i);
						obj.lineKey = record.getSublistValue('custpage_po_lines','custpage_po_linekey', i);
						obj.custPN = record.getSublistValue('custpage_po_lines','custpage_cust_pn', i);
						obj.soLineKey = record.getSublistValue('custpage_po_lines','custpage_so_line_unique_key', i);
						lines.push(obj);
						if(!isNull(origLines)){
							parsedOrigLines.push(obj);
						}
//						alert('New Obj - ' + JSON.stringify(obj));

					}
				}
				if(!isNull(origLines)){
					if(parsedOrigLines.length > 0){
						record.setValue('custpage_selected_lines',JSON.stringify(parsedOrigLines));
//						alert(JSON.stringify(parsedOrigLines));
						return parsedOrigLines;
					}
				}
				record.setValue('custpage_selected_lines',JSON.stringify(lines));
				return lines;
				
			}catch(e){
				alert('getSelectedLines Error - ' + e.message)
			}
		}
		
		function applyFilters(context){
			try{
				jQuery('#custpage_add_filter_fs').click();
				jQuery('#submitter').click();

			}catch(e){
				
			}
		}
		
		
		function refreshPage(params){
			var output;
			if(params == null){
				output = url.resolveScript({
				    scriptId: 'customscript_trans_consolidated',
				    deploymentId: 'customdeploy_trans_consolidated',
				    returnExternalUrl: false			
				});
				
//				return;
			}else{
				output = url.resolveScript({
				    scriptId: 'customscript_trans_consolidated',
				    deploymentId: 'customdeploy_trans_consolidated',
				    returnExternalUrl: false,
				    params: {'selectedLines':params}
				});
			}
			
			window.location.href = output;
		}
		
		function saveRecord(context){
			var record = context.currentRecord;
			var addFilter = record.getValue('custpage_add_filter');
//			alert('PO Num - ' + poNum + ' Add Filter - ' + addFilter);
			if(addFilter){
				var selectedLines = getSelectedLines(record);
				//alert(JSON.stringify(selectedLines));
			}else{
				jQuery('#submitter').addClass("wait");
				jQuery("html").addClass("wait");
				jQuery('.wait').css("cursor", "wait");
			}
			
			return true;
			
		}
		
		function pageInit(){
			jQuery('#custpage_add_filter_fs').css("display", "none");
			jQuery('#custpage_add_filter_fs_lbl').css("display", "none");
			
		}
		
		function isNull(value){
			if(value == null || value == '' || value == undefined){
				return true;
			}
			
			return false;
		}
        
        
        return {
        	fieldChanged: fieldChanged,
        	pageInit: pageInit,
        	saveRecord: saveRecord,
        	refreshPage: refreshPage,
        	applyFilters: applyFilters
        };
    });

