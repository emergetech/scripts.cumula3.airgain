/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 * @NScriptType ClientScript
 * 
 */

define(['N/record','N/runtime','N/url'],
    function (rec,runtime,url)
    {
		function createTransactions(transactionId,tranType,date,ctNum){
			try{
				
				var output;
				if(tranType == null){
					output = url.resolveScript({
					    scriptId: 'customscript_create_ct_invoice_pdf',
					    deploymentId: 'customdeploy_create_ct_invoice_pdf',
					    returnExternalUrl: false,
					    params: {'transactionId':transactionId,'type':'shipping'}
					});
				}else{
					output = url.resolveScript({
					    scriptId: 'customscript_create_invoices_bills',
					    deploymentId: 'customdeploy_create_invoices_bills',
					    returnExternalUrl: false,
					    params: {'tranType':tranType,'transactionId':transactionId,'tranDate':date,'ctNum':ctNum}
					});
				}
				
					
					
				//window.location.href = output;
				window.open(output,'_self');

			}catch(e){
				alert('Error - ' + e.message);
			}
		}
		
		function deleteTransactions(transactionId){
			try{
				var c = confirm('Are you sure you want to delete this record?');

				if(c){
					var output = url.resolveScript({
					    scriptId: 'customscript_delete_ct_lines',
					    deploymentId: 'customdeploy_delete_ct_lines',
					    returnExternalUrl: false,
					    params: {'transactionId':transactionId}
					});
					
					window.location.href = output;
				}
			}catch(e){
				alert('error - ' + e.message);
			}

		}
		
		function openQuoteForms(transactionId,quoteForm){
			try{
				output = url.resolveScript({
				    scriptId: 'customscript_generate_quote_pdf',
				    deploymentId: 'customdeploy_generate_quote_pdf',
				    returnExternalUrl: false,
				    params: {'quoteForm':quoteForm,'transactionId':transactionId}
				});
				window.open(output,'_self');
			}catch(e){
				alert('error - ' + e.message);
			}
		}
		
		function onFieldChange(context){
			try{
				var record = context.currentRecord;
				if(context.fieldId == 'custpage_ship_to_options'){
					//alert('hello');
					var addr = record.getValue('custpage_ship_to_options');
					record.setValue('custrecord_ship_to_addr',addr);
				}
				
			}catch(e){
				alert(e);
			}
		}

		function pageInit(){
			
		}
        
        
        return {
        	pageInit: pageInit,
        	fieldChanged: onFieldChange,
        	createTransactions: createTransactions,
        	deleteTransactions: deleteTransactions,
        	openQuoteForms: openQuoteForms
        };
    });

