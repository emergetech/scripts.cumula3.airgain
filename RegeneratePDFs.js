/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * 
 */

define(['N/record','N/search','N/file','N/redirect','N/runtime'],
    function (rec,search,file,redirect,runtime)
    {
        function afterSubmit(context)
        {
        	try{   
        		if(runtime.executionContext != 'USERINTERFACE'){
        			log.debug('afterSubmit','Not created in user interface, exiting script');
        			return;
        		}
	        	var newRec = context.newRecord;
	        	var oldRec = context.oldRecord;
	        	
	        	var ctpdf = newRec.getValue('custrecord_ct_pdf');
	        	var proforma = newRec.getValue('custrecord_proforma_inv');
	        	var contractToSell = newRec.getValue('custrecord_contract_to_sell');
	        	var shippingNotice = newRec.getValue('custrecord_shipping_notice');
	        	
	        	if(isNull(ctpdf) && isNull(proforma) && isNull(contractToSell) && isNull(shippingNotice)){
	        		return;
	        	}

	        	var recChange = checkRecFields(newRec,oldRec);//false;        		        	
	        	log.debug('afterSubmit','Record Changed - ' + recChange);
	        	
	        	if(!recChange){
	        		return;
	        	}
	        	
	        	regeneratePDFs(newRec)

	        	
        	}catch(e){
        		log.error('afterSubmit','Error - ' + e.message);
        	}
           
        }
        
        /**
         * Deletes the PDF fields if there are any and calls the Suitelet that will
         * re-create the PDF.
         * @param record
         */
        function regeneratePDFs(record){
        	var transactionId = record.getValue('id');
        	var createCTInv = false;
        	var createShipPDF = false;
        	var ctPDF = record.getValue('custrecord_ct_pdf');
        	if(!isNull(ctPDF)){
        		createCTInv = true;
        		file.delete({
    				id: ctPDF
    			})
        	}
        	
        	var pdfIds = ['custrecord_proforma_inv','custrecord_contract_to_sell','custrecord_shipping_notice'];
        	var shipPDF = record.getValue('custrecord_shipping_notice');
        	if(!isNull(shipPDF)){
        		createShipPDF = true;
            	for(var i = 0; i < pdfIds.length; i++){
        		var pdfId = record.getValue(pdfIds[i]);
        		log.debug('regeneratePDFs','Pdf Id - ' + pdfId);
        		if(!isNull(pdfId)){
        			file.delete({
        				id: pdfId
        			})
        		}
            	}
        	}
        	
        	if(createCTInv == false && createShipPDF == false){
        		return;
        	}

        	redirect.toSuitelet({
        		scriptId: 'customscript_create_ct_invoice_pdf',
        		deploymentId: 'customdeploy_create_ct_invoice_pdf',
        		parameters: {'transactionId':transactionId,'type':null,'createShipPDF':createShipPDF,'createCTInv':createCTInv}
        	});
        }
        
        /**
         * Checks the old record against the new record and returns
         * true if any of the field values do not match
         * 
         * @param newRec
         * @param oldRec
         */
        function checkRecFields(newRec,oldRec){
        	var newFields = newRec.getFields();
        	var oldFields = oldRec.getFields();
        	for(var k = 0; k < newFields.length; k++){
        		var newField = newFields[k];
        		
        		/*
        		 * If "custrecord" is not in the beginning of the field id it is not a custom field.
        		 * Needed because there are native netsuite fields that change with each submit
        		 */ 
        		if(newField.indexOf('custrecord') != 0){
        			continue;
        		}

        		var newFieldValue = newRec.getValue(newField);
	        	for(var i = 0; i < oldFields.length; i++){
	        		var oldField = oldFields[i];
	        		var oldFieldValue = oldRec.getValue(oldField);

        			if(newField == oldField && checkFieldType(newField,newFieldValue,newRec) != checkFieldType(oldField,oldFieldValue,newRec)){
        				recChange = true;
		        		log.debug('afterSubmit','Fields - ' + newField);
		        		log.debug('afterSubmit','Old Value - ' + oldFieldValue + ' New Value - ' + newFieldValue);
		        		return true;
	        		}	        		
	        	}
        	}
        	
        	return false;
        }
        
        /**
         * Checks the field type of the field, if it is a date
         * it is formatted so the value can be determined if there
         * was a change. Returns the field value.
         * 
         * @param field
         * @param fieldValue
         * @param newRec
         */
        function checkFieldType(field,fieldValue,newRec){
        	try{
        		var objField = newRec.getField({
        			fieldId: field });
        		if(isNull(objField)){
        			return null;
        		}
        		var fieldType = objField.type;
        		if(fieldType == 'date'){
        			return formatDate(fieldValue);
        		}else{
        			return fieldValue;
        		}
        	}catch(e){
        		log.error('checkFieldType','Error - ' + e.message);
        	}
        }
        
        function formatDate(date){
    		if(isNull(date)){
    			return null;
    		}
    		
			var month = date.getMonth() + 1;
			var day = date.getDate();
			var year = date.getFullYear();

			return month+'/'+day+'/'+year;	
        }


        /**
         * If the value sent in is null or empty the function
         * returns true.
         * 
         * @param value
         */
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
        
        return {
        	afterSubmit: afterSubmit
        };
    });

