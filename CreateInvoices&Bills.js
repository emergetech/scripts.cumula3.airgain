/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * 
 * AIRGAIN-3
 */
define(['N/redirect','N/search','N/record','/SuiteScripts/Cumula 3/Search-20','N/task','N/runtime', '/SuiteScripts/Cumula 3/SearchForConsolidateLines'],
    function (redirect,search,record,searchScript,task,runtime,lineItems)
    {
		var INV_ERROR = false;
        function run(context)
        {
            try{
                if (context.request.method === 'GET'){
                	var transactionId = context.request.parameters.transactionId;
                	var tranType = context.request.parameters.tranType;
                	var tranDate = context.request.parameters.tranDate;
                	var ctNum = context.request.parameters.ctNum;
                	
                	log.debug('run','Tran Type - ' + tranType + ' CT Id - ' + transactionId + ' Date - ' + tranDate);
                	groupLines(transactionId,tranType,tranDate,ctNum);


                	if(tranType == 'invoice' && INV_ERROR != true){
                		//log.debug('run','Redirecting to suitelet');
                		redirect.toSuitelet({
                    		scriptId: 'customscript_create_ct_invoice_pdf',
                    		deploymentId: 'customdeploy_create_ct_invoice_pdf',
                    		parameters: {'transactionId':transactionId,'type':'consolidated'}
                    	});
                	}else{
                    	redirect.toRecord({
	            			type : 'customrecord_consolidated_transaction',
	            			id : transactionId,
	            			isEditMode: false
                    	});
                	}
                	


                }
                
            }catch(e){
                log.error("run", "Error - " + e);
            }
        }
        
        function groupLines(transactionId,tranType,tranDate,ctNum){
        	var mainFilter = null;
        	if(tranType == 'bill'){
        		mainFilter = 'po';
        	}
        	var searchResults = lineItems.SearchLines(transactionId,mainFilter);
        	
            var prevTransaction = '';
            var poArr = [];
            var data = [];
            var invNum = 1;               
            for(var i = 0; i < searchResults.length; i++){
            	var transaction;
            	if(tranType == 'bill'){
            		transaction = searchResults[i].getValue('custrecord_ct_po_id');
            	}else{
            		transaction = searchResults[i].getValue('custrecord_ct_so_id');
            	}
            	
            	log.debug('groupLines','Transaction - ' + transaction);
            	
            	if(transaction != prevTransaction && prevTransaction != ''){
            		if(tranType == 'bill'){
            			createVendorBill(poArr,prevTransaction,transactionId);
            		}else{
            			createInvoice(poArr,prevTransaction,transactionId,tranDate,invNum,ctNum);
            			invNum++;
            		}
            		//createTransaction(poArr,transactionId,tranType);
            		poArr = [];
            	}

            	if(tranType == 'bill'){
            		poArr.push(searchResults[i].getValue('custrecord_ct_line_id'));
        		}else{
        			poArr.push(searchResults[i]);
        		}
            	
            	
            	
            	if(i == searchResults.length - 1){
            		if(tranType == 'bill'){
            			createVendorBill(poArr,transaction,transactionId);
            		}else{
            			createInvoice(poArr,transaction,transactionId,tranDate,invNum,ctNum);
            			invNum++;
            		}
//            		createTransaction(poArr,transactionId,tranType);
            	}
            	prevTransaction = transaction;
            }
            
            
        	
        }
        
        function createVendorBill(data,poId,ctId){
        	try{
        		log.debug('createVendorBill','Data - ' + data);
        		var bill = record.transform({
					fromType: record.Type.PURCHASE_ORDER, 
					fromId: poId,
					toType: record.Type.VENDOR_BILL, 
					isDynamic: false
				});
        		// Load the Consolidated Transaction record to get the felds that need to be populated on the Bill
        		var ctRec = record.load({
					type: 'customrecord_consolidated_transaction', 
					id: ctId
				});
        		bill.setValue('custbody_consolidated_trans',ctId);
        		bill.setValue('account','821');
        		var tranId = ctRec.getValue('custrecord_ct_vendor_inv_num');
        		if(hasValue(tranId)){
            		bill.setValue('tranid',tranId);
        		}
        		var tranDate = ctRec.getValue('custrecord_ct_vendor_invoice_date');
        		if(hasValue(tranDate)){
        			bill.setValue('trandate',tranDate);
        		}
        		var dueDate = ctRec.getValue('custrecord_ct_vendor_inv_due_date');
        		if(hasValue(dueDate)){
        			bill.setValue('duedate',dueDate);
        		}
        		
        		bill.setValue('memo',ctRec.getValue('custrecord_ct_notes'));
        		var x = bill.getLineCount('item') - 1;
        		for(var i=x;i >= 0;i--){
        	//	for(var i = 0; i < bill.getLineCount('item'); i++){
        			var orderLine = bill.getSublistValue('item','orderline',i);
        			log.debug('createVendorBill','Order Line - ' + orderLine);

        			if(data.indexOf(orderLine) < 0){
        				bill.removeLine({
    					    sublistId: 'item',
    					    line: i,
    					    ignoreRecalc: true
    					});
        			}

        		}
        		
        		var id = bill.save();
        		log.debug('createVendorBill','Id - ' + id);
        	}catch(e){
        		log.error('createVendorBill','Error - ' + e.message);
        	}
        }
        
        function createInvoice(data,soId,ctId,tranDate,invNum,ctNum){
        	try{
        		var invoice = record.transform({
					fromType: record.Type.SALES_ORDER, 
					fromId: soId,
					toType: record.Type.INVOICE, 
					isDynamic: false
				});
        		invoice.setValue('custbody_consolidated_trans',ctId);
        		var tranId = ctNum+'-'+invNum;
        		log.debug('createInvoice','TranId - ' + tranId);
        		invoice.setValue('tranid',tranId);
        		invoice.setValue('account','817');
        		if(hasValue(tranDate)){
        			var date = formatDate(new Date(tranDate));
            		invoice.setValue('trandate',new Date(date));
        		}
        		var lineCnt = invoice.getLineCount('item');
        		var uniqueKeys = [];
        		for(var i = 0; i < lineCnt; i++){
        			var item = invoice.getSublistValue('item','item',i);
        			var qty = invoice.getSublistValue('item','quantity',i);
        			var po = invoice.getSublistValue('item','createpo',i);
        			var uniqueKey = invoice.getSublistValue('item','custcol_so_line_unique_key',i);
        			log.debug('createInvoice','Item - ' + item + ' Unique Key - ' + uniqueKey + ' Qty - ' + qty);
        			
        			for(var k = 0; k < data.length; k++){
        				if(uniqueKey == data[k].getValue('custrecord_so_line_unique_key')){

        					var qty = data[k].getValue('custrecord_ct_qty');
        					var amount = data[k].getValue('custrecord_ct_amount');
        					invoice.setSublistValue('item','quantity',i,qty);
        					invoice.setSublistValue('item','amount',i,amount);
        					uniqueKeys.push(invoice.getSublistValue('item','lineuniquekey',i));
        				}
        			}
        		}
        		
        		//for(var j = 0; j < lineCnt; j++){
        		var x = lineCnt - 1;
        		for(var j=x;j >= 0;j--){

        			var uniqueKey = invoice.getSublistValue('item','lineuniquekey',j);
        			if(uniqueKeys.indexOf(uniqueKey) < 0){
        				invoice.removeLine({
    					    sublistId: 'item',
    					    line: j,
    					    ignoreRecalc: true
    					});
        			}
        		}
        		var invId = invoice.save();
        		log.debug('createInvoice','Invoice Id - ' + invId);
        	}catch(e){
        		log.error('createInvoice','Error - ' + JSON.stringify(e));
        		INV_ERROR = true;
        	}
        }
        

		/******************************************************************
		 * If the value sent in is an empty string or undefined return a 
		 * null value. 
		 *
		 * @param value
		 ******************************************************************/
        function isEmpty(value){
            if(value == "" || value == undefined){
                return null
            }
            
            return value;
        }
        
		/******************************************************************
		 * If the value sent in is not null or an empty string, return true. 
		 *
		 * @param value
		 ******************************************************************/
    	function hasValue(value){
    		if(value != null && value != '' && value != undefined){
    			return true;
    		}
    		
    		return false;
    	}
    	
    	/******************************************************************
    	 * Formats the Consolidated Transaction date to the M/D/YYYY format.
    	 *
    	 ******************************************************************/
        function formatDate(date){
        	try{
				var month = date.getMonth() + 1;
				var day = date.getDate();
				var year = date.getFullYear();
				
				var dateOnly = month+'/'+day+'/'+year;	
				log.debug('formatDate', 'New Date - ' + dateOnly);
				return dateOnly; //month+'/'+day+'/'+year;	
			}catch(e){
				log.error('formatDate','Error - ' + e.message);
			}
        }
        
        return {
            onRequest: run,
            
        };
    });