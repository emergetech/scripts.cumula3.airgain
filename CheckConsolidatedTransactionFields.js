/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * Checks if the Dates on the Consolidated Transaction have changed. If they have, it calls
 * the Map/Reduce script that updates the date on the related records.
 */

define(['N/record','N/runtime','N/task'],
    function (record,runtime,task)
    {
        function onAfterSubmit(context)
        {
        	try{
        		if (context.type === context.UserEventType.CREATE || runtime.executionContext != 'USERINTERFACE' || context.type === context.UserEventType.DELETE){
					log.debug('onAfterSubmit','Record is on deletion, creation or not on userinterface, exiting script')
					return;
				}
	        	var rec = context.newRecord;
				var oldRec = context.oldRecord;
				var recId = rec.getValue('id');
	        	
				var oldInvDate = oldRec.getValue('custrecord_ct_invoice_date');
				var newInvDate = rec.getValue('custrecord_ct_invoice_date');
				
				var oldBillDate = oldRec.getValue('custrecord_ct_vendor_invoice_date');
				var newBillDate = rec.getValue('custrecord_ct_vendor_invoice_date');
				
				var oldTranDate = oldRec.getValue('custrecord_ct_date');
				var newTranDate = rec.getValue('custrecord_ct_date');
				
				if(isNull(newInvDate) && isNull(newBillDate) && isNull(newTranDate)){
					log.debug('onAfterSubmit','All dates are empty, exiting script');
					return;
				}
				
				log.debug('onAfterSubmit','Old Date - ' + oldInvDate);			
				log.debug('onAfterSubmit','New Date - ' + newInvDate);
				
				if(formatDate(oldInvDate) != formatDate(newInvDate)){
                	for(var i = 1; i < 4; i++){
						var deploymentId = 'customdeploy_change_trans_date_'+i;
						var success = callMapReduceScript(newInvDate,recId,'invoice',deploymentId);
						
						if(!success){
							continue;
						}
						
						break;
					}
				}
				
				
				log.debug('onAfterSubmit','Old Bill Date - ' + formatDate(oldBillDate));			
				log.debug('onAfterSubmit','New Bill Date - ' + formatDate(newBillDate));
				
				if(formatDate(oldBillDate) != formatDate(newBillDate)){
                	for(var i = 1; i < 4; i++){
						var deploymentId = 'customdeploy_change_trans_date_'+i;
						var success = callMapReduceScript(newBillDate,recId,'vendorbill',deploymentId);
						
						if(!success){
							continue;
						}
						
						break;
					}
					
				}
				
				log.debug('onAfterSubmit','Old Tran Date - ' + formatDate(oldTranDate));			
				log.debug('onAfterSubmit','New Tran Date - ' + formatDate(newTranDate));
				if(formatDate(oldTranDate) != formatDate(newTranDate)){
                	for(var i = 1; i < 4; i++){
						var deploymentId = 'customdeploy_change_trans_date_'+i;
						var success = callMapReduceScript(newTranDate,recId,'fulfillment',deploymentId);
						
						if(!success){
							continue;
						}
						
						break;
					}
				}
	        	
        	}catch(e){
        		log.error('onAfterSubmit','Error - ' + e.message);
        	}
           
        }
        
        function formatDate(dateValue){
          		var date = new Date(dateValue);
 				var month = date.getMonth() + 1;
 				var day = date.getDate();
 				var year = date.getFullYear();

 				return month+'/'+day+'/'+year;	
        }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
        
		function callMapReduceScript(date,recId,dateType,deploymentId){
			try{
				log.debug('map','Calling Map/Reduce Script - ' + deploymentId);
	    		var mrTask = task.create({taskType: task.TaskType.MAP_REDUCE});
	    		mrTask.scriptId = 'customscript_change_trans_date';
	    		mrTask.deploymentId = deploymentId;
	    		mrTask.params = {'custscript_new_date': date, 'custscript_date_type':dateType, 'custscript_ct_id':recId};
	    		var mrTaskId = mrTask.submit();
	    		return true;
			}catch(e){
				log.error('callMapReduceScript','Error - ' + e.message);
				return false;
			}
		}
        
        return {
        	afterSubmit: onAfterSubmit
        };
    });

