/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * 
 */
define(['N/ui/serverWidget','N/search','N/record','/SuiteScripts/Cumula 3/Search-20','N/task','N/runtime', '/SuiteScripts/Cumula 3/SearchForConsolidateLines'],
    function (ui,search,record,searchScript,task,runtime,lineItems)
    {
        function run(context)
        {
            try{
                if (context.request.method === 'GET'){
                	var transactionId = context.request.parameters.transactionId;
                	log.debug('run','Transaction Id - ' + transactionId);
                	buildUserForm(context);
                	for(var i = 1; i < 4; i++){
						var deploymentId = 'customdeploy_delete_ct_lines_mr_'+i;
						var success = callMapReduceScript(transactionId,deploymentId);
						
						if(!success){
							continue;
						}
						
						break;
					}
                }
                
            }catch(e){
                log.error("run", "Error - " + e);
            }
        }
        
        function buildUserForm(context){
        	var form = ui.createForm({
                title: 'Delete Consolidated Transaction'
            });
            var message = form.addField({
                id: 'custpage_message',
                label: 'Consolidated Message',
                type: ui.FieldType.TEXT
            }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE })
            message.defaultValue = 'The transaction is in progress to be deleted. This window can be closed.';
           
            context.response.writePage(form);
        }
        

		function callMapReduceScript(ctId,deploymentId){
			try{
				log.debug('map','Calling Map/Reduce Script - ' + deploymentId);
	    		var mrTask = task.create({taskType: task.TaskType.MAP_REDUCE});
	    		mrTask.scriptId = 'customscript_delete_ct_lines_mr';
	    		mrTask.deploymentId = deploymentId;
	    		mrTask.params = {'custscript_consolidated_transaction': ctId};
	    		var mrTaskId = mrTask.submit();
			}catch(e){
				log.error('callMapReduceScript','Error - ' + e.message);
			}
		}
        
        return {
            onRequest: run,
            
        };
    });