/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 * @NScriptType MapReduceScript
 * 
 * Changes the date on the transactions tied to the Consolidated Transaction to the date on the Consolidated Transaction.
 * 
 */
define(['N/search', 'N/record', 'SuiteScripts/Cumula 3/Search-20', 'N/runtime', 'N/file'],
    function(search, record, searchScript, runtime, file)
    { 	

        function getInputData()
        {
            try{
           
            	var scriptObj = runtime.getCurrentScript();
            	var ctId = scriptObj.getParameter({name: 'custscript_ct_id'});
                var dateType = scriptObj.getParameter({name: 'custscript_date_type'});
                log.debug('getInputData','Ct id - ' + ctId + ' Date Type - ' + dateType);      
                
                var records = getRelatedTransactions(ctId,dateType);
                log.debug('getInputData','Records - ' + JSON.stringify(records));
                return records;
            }catch(e){
                log.error('getInputData','Error - ' + e.message);
            }

        }

        function map(context)
        {
        	try{
                var searchResult = JSON.parse(context.value);
                log.debug('map','Search Result - ' + JSON.stringify(searchResult));

                var recId = searchResult.id;
                var recType = searchResult.recordType;
                
                var rec = record.load({
                	type: recType,
	        		id:recId
                })
                var scriptObj = runtime.getCurrentScript();
            	var date = scriptObj.getParameter({name: 'custscript_new_date'});
                log.debug('getInputData','New Date - ' + date);
                
                rec.setValue('trandate',new Date(date));
                rec.save();

                log.debug('map','Rec Id - ' + recId + ' Rec Type - ' + recType);


        	}catch(e){
        		log.error('map','Error - ' + e.message);
        	}
        }
        
        function getRelatedTransactions(ctId,dateType){
        	var filters = [
               search.createFilter({
            	   name: 'custbody_consolidated_trans',
            	   operator : search.Operator.ANYOF,
            	   values: ctId
               }),
               search.createFilter({
            	   name: 'mainline',
            	   operator : search.Operator.IS,
            	   values: true
               })
           ];

        	var newSearch = searchScript.init('transaction',null,filters);
            var searchResults = newSearch.getResults();
            log.debug('getRelatedTransactions','Results - ' + JSON.stringify(searchResults));
            var recIds = [];
            var prevRecId = '';
            for(var i = 0; i < searchResults.length; i++){
            	var recType = searchResults[i].recordType;
            	if(recType == 'purchaseorder'){
            		prevRecId = recId;
            		continue;
            	}
            	if(dateType == 'invoice'){
            		if(recType != 'invoice'){
            			prevRecId = recId;
            			continue;
            		}
            	}
            	else if(dateType == 'vendorbill'){
            		if(recType != 'vendorbill'){
            			prevRecId = recId;
            			continue;
            		}
            	}
            	else{
            		if(recType == 'vendorbill' || recType == 'invoice'){
            			prevRecId = recId;
            			continue;
            		}
            	}
            	
            	var recId = searchResults[i].id;
            	if(recId != prevRecId){
            		recIds.push(searchResults[i]);
            	}
            	
            	prevRecId = recId;
            	
            }
            
            return recIds;
        }
        
        function summarize(){
        	
        }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
      
       
        return {
            getInputData: getInputData,
            map: map,
            //reduce: reduce,
            summarize: summarize
        };
    });