/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * 
 * Creates the view that allows the users to choose what lines will be used to create the Consolidated Transaction. 
 * 
 * AIRGAIN-3 : AIRGAIN-7 : AIRGAIN-8
 */
define(['N/ui/serverWidget','N/search','N/record','/SuiteScripts/Cumula 3/Search-20','N/task','N/runtime','/SuiteScripts/Cumula 3/UniqueNumber-20', 'N/format'],
    function (ui,search,record,searchScript,task,runtime,uniqueNum, format)
    {
		var ERROR_DATA = [];
		var LINE_LENGTH;
        function run(context)
        {
            try{
                if (context.request.method === 'GET'){
                	var poNum = context.request.parameters.poNum;
                	var selectedLines = context.request.parameters.selectedLines;
                	var filters = context.request.parameters.filter;
                    if(filters){
//                		log.debug('run','Filter conditional');
                		return
                	}
//                	log.debug('run','PO Num - ' + poNum);
                	log.debug('run','Selected Lines  - ' + selectedLines);
                    buildForm(context,poNum,selectedLines);
                    
                }
                
                if(context.request.method === 'POST'){
                	//log.debug('run','Context Request - ' + context.request.parameters.custpage_po_linesdata);
          
                	var addFilter = context.request.parameters.custpage_add_filter;
                	if(addFilter != 'T'){
                		log.debug('run','Lines - ' + context.request.parameters.custpage_selected_lines);
                		var lines = context.request.parameters.custpage_selected_lines;
                		var data = context.request.parameters.custpage_po_linesdata;
            			if(data != ''){
            				getSelectedLines(context,lines);
            				log.debug('run','Error Data Final - ' + JSON.stringify(ERROR_DATA));
            				buildResponseForm(context);
            				return;
            			}
                	}else{
                		var selectedLines = context.request.parameters.custpage_selected_lines;
                		var changedQtys = context.request.parameters.custpage_changed_qtys;
                		log.debug('run','Lines Filter - ' + context.request.parameters.custpage_selected_lines);
                    	var obj = {};
                    	obj.tranid = isEmpty(context.request.parameters.custpage_po_num);
                    	obj.trandate = isEmpty(context.request.parameters.custpage_date);
                    	obj.mainname = isEmpty(context.request.parameters.custpage_vendor);
                    	obj.createdFrom = isEmpty(context.request.parameters.custpage_customer);
                    	obj.custbody_order_reference = isEmpty(context.request.parameters.custpage_order_ref_num);
                		//buildForm(context,po,selectedLines,date,vendor,customer);
                    	buildForm(context,obj,selectedLines,changedQtys);
                	}
                	//log.debug('run','Filter conditional POST - ' + filters);
                	
                }


            }catch(e){
                log.error("run", "Error - " + e);
            }
            
           
        }
        
		/******************************************************************
		 * Builds the form for the user to view in the UI
		 * @param context
		 ******************************************************************/
        function buildForm(context,filterObj,selectedLines,changedQtys){    	
            try{//context,obj,selectedLines

                var form = ui.createForm({
                    title: 'Create Consolidated Transaction'
                });

                form.addField({
                    id : 'custpage_vendor',
                    type : ui.FieldType.SELECT, 
                    label : 'Vendor',
                    source: 'vendor'
                });
                form.addField({
                    id : 'custpage_customer',
                    type : ui.FieldType.SELECT, 
                    label : 'Customer',
                    source: 'customer'
                });
                form.addField({
            		id : 'custpage_po_num',
            		type : ui.FieldType.TEXT, 
            		label : 'PO #'
            	});
                form.addField({
                    id : 'custpage_date',
                    type : ui.FieldType.DATE, 
                    label : 'Date'
                });
                form.addField({
                    id : 'custpage_order_ref_num',
                    type : ui.FieldType.SELECT, 
                    label : 'Order Reference Number',
                    source: 'customrecord_order_reference'
                });
                var lineField = form.addField({
                    id : 'custpage_selected_lines',
                    type : ui.FieldType.LONGTEXT, 
                    label : 'Selected Lines'
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                var changedQtysField = form.addField({
                    id : 'custpage_changed_qtys',
                    type : ui.FieldType.LONGTEXT, 
                    label : 'Changed Qtys'
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                form.addField({
                    id : 'custpage_add_filter',
                    type : ui.FieldType.CHECKBOX, 
                    label : 'Selected Line Keys'
                })

                if(hasValue(selectedLines)){
                	log.debug('buildForm','Has Value Selected Lines - ' + selectedLines);
                	var selectedLinesValue = JSON.parse(selectedLines);
                	lineField.defaultValue = selectedLines;
                }
                if(hasValue(changedQtys)){
                	log.debug('buildForm','Has Value Changed Qtys - ' + changedQtys);
                	var selectedLinesValue = JSON.parse(changedQtys);
                	changedQtysField.defaultValue = changedQtys;
                }

                var sublist = form.addSublist({
                    id : 'custpage_po_lines',
                    type : ui.SublistType.LIST, 
                    label : 'Available PO Lines'
                }); //ui.SublistType.LIST
                sublist.addField({
                    id: 'custpage_line_selected',
                    label: 'Select',
                    type: ui.FieldType.CHECKBOX
                });

                sublist.addField({
                    id: 'custpage_po_cust',
                    label: 'Customer',
                    type: ui.FieldType.SELECT,
                    source: 'customer'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE });

                sublist.addField({
                    id: 'custpage_po_vendor',
                    label: 'Vendor',
                    type: ui.FieldType.SELECT,
                    source: 'vendor'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE });

                sublist.addField({
                    id: 'custpage_po_date',
                    label: 'PO Date',
                    type: ui.FieldType.DATE
                });
                sublist.addField({
                    id: 'custpage_ship_date',
                    label: 'Ship Date',
                    type: ui.FieldType.DATE
                });
                sublist.addField({
                    id: 'custpage_po_number',
                    label: 'PO#',
                    type: ui.FieldType.SELECT,
                    source: 'transaction'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE })

                sublist.addField({
                    id: 'custpage_so_num',
                    label: 'SO#',
                    type: ui.FieldType.SELECT,
                    source: 'transaction'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE });

                sublist.addField({
                    id: 'custpage_item',
                    label: 'Item',
                    type: ui.FieldType.SELECT,
                    source: 'item'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE })

                sublist.addField({
                    id: 'custpage_qty',
                    label: 'Qty',
                    type: ui.FieldType.FLOAT
                }).updateDisplayType({displayType: ui.FieldDisplayType.ENTRY })
                sublist.addField({
                    id: 'custpage_orig_qty',
                    label: 'Original QTY',
                    type: ui.FieldType.FLOAT
                })
                sublist.addField({
                    id: 'custpage_max_qty',
                    label: 'Max Qty',
                    type: ui.FieldType.FLOAT
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN })
                sublist.addField({
                    id: 'custpage_qty_received',
                    label: 'QTY Received',
                    type: ui.FieldType.FLOAT
                })
                sublist.addField({
                    id: 'custpage_rate',
                    label: 'Rate',
                    type: ui.FieldType.FLOAT
                });
                sublist.addField({
                    id: 'custpage_order_reference',
                    label: 'Order Reference',
                    type: ui.FieldType.TEXT
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE })
                sublist.addField({
                    id: 'custpage_po_line_num',
                    label: 'PO Line No.',
                    type: ui.FieldType.TEXT
                })
                var lineKey = sublist.addField({
                    id: 'custpage_po_linekey',
                    label: 'Line Key',
                    type: ui.FieldType.TEXT
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });

                sublist.addField({
                    id: 'custpage_po_lineid',
                    label: 'Line Id',
                    type: ui.FieldType.TEXT
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                sublist.addField({
                    id: 'custpage_cust_pn',
                    label: 'Customer Part Number',
                    type: ui.FieldType.SELECT,
                    source: 'customrecord_scm_customerpartnumber'
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                sublist.addField({
                    id: 'custpage_so_line_unique_key',
                    label: 'SO Line Unique Key',
                    type: ui.FieldType.TEXT
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                var eor = sublist.addField({
                    id: 'custpage_eor',
                    label: 'EOR',
                    type: ui.FieldType.TEXT
                }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN });
                eor.defaultValue = 'EOR';

                sublist.addMarkAllButtons();
                
                form.clientScriptModulePath = 'SuiteScripts/Cumula 3/ConsolidatedTransactionView-Client.js';
                form.addButton({id : 'apply_filters', label : 'Apply Filters', functionName: 'applyFilters()'});
                var poLines = getPOLines(filterObj,selectedLines);
//                log.debug('buildForm','Returned - ' + JSON.stringify(poLines))
                populateSublist(sublist,poLines,selectedLines,changedQtys);
                form.addSubmitButton({ label : 'Create Consolidated Transactions' });

                context.response.writePage(form);
            }catch(e){
                log.error('buildForm','Error - ' + e.message);
            }
            
        }
        
		/******************************************************************
		 * Loads the 'Consolidated Transaction Results - DO NOT DELETE'
		 * saved search and returns the results.
		 * 
		 * @param poNum
		 *
		 ******************************************************************/
        function getPOLines(filterObj,selectedLines){
            try{                
                var poSearch = search.load({
                	id: 'customsearch_trans_consolidated_results' });
            	
                var defaultFilters = poSearch.filters;
                var customFilters = [];
                /*
                 * Adds filters to the results depending on if there were values in the
                 * Filter options on the suitelet
                 */
                for(var filter in filterObj) {
                    if(filterObj.hasOwnProperty(filter)) {
                        var filterValue = filterObj[filter];
                        if(hasValue(filterValue)){
                        	if(filter == 'createdFrom'){
                        		customFilters.push(
                    				search.createFilter({
            	 	                    name : 'entity',
            	 	                    join: filter,
            	 	                    operator : search.Operator.ANYOF,
            	 	                    values : filterValue
            	                     })
                        		);
                        	}
                        	if(filter == 'custbody_order_reference'){
                        		customFilters.push(
                    				search.createFilter({
            	 	                    name : filter,
            	 	                    join: 'createdFrom',
            	 	                    operator : search.Operator.ANYOF,
            	 	                    values : filterValue
            	                     })	
                            	);
                        	}
                        	if(filter == 'mainname'){
                        		customFilters.push(
                    				search.createFilter({
            	 	                    name : filter,
            	 	                    operator : search.Operator.ANYOF,
            	 	                    values : filterValue
            	                     })	
                            	);
                        	}
                        	if(filter == 'trandate'){
                        		customFilters.push(
                    				search.createFilter({
            	 	                    name : filter,
            	 	                    operator : search.Operator.ON,
            	 	                    values : filterValue
            	                     })	
                            	);
                        	}
                        	if(filter == 'tranid'){
                        		customFilters.push(
                    				search.createFilter({
            	 	                    name : filter,
            	 	                    operator : search.Operator.IS,
            	 	                    values : filterValue
            	                     })	
                            	);
                        	}
                        }

                    }
                }
                
                if(customFilters.length > 0){
                	log.debug('getPOLines','Custom Filters - ' + customFilters);
                	defaultFilters = defaultFilters.concat(customFilters);
                    poSearch.filters = defaultFilters;
                }

                var newSearch = searchScript.init(null,'customsearch_trans_consolidated_results',poSearch.filters);
                var searchResults = newSearch.getResults();
               // log.audit('getPOLines','Results - ' + JSON.stringify(searchResults));
                
                var results = [];
                var poIds = [];
                var previousLineKey = '';
                for(var i = 0; i < searchResults.length; i++){
                	var lineKey = searchResults[i].getValue('lineuniquekey');
                	//if(searchResults[i].getValue('quantityshiprecv') != searchResults[i].getValue('quantity')){
                		if(lineKey != previousLineKey){
	                		results.push(searchResults[i]);
	                		poIds.push(searchResults[i].id);
                		}
                	//}
                    previousLineKey = lineKey;        	
                }
                log.audit('getPOLines','Results After filtering - ' + JSON.stringify(results));
//                log.audit('getPOLines','Results After filtering Length - ' + results.length);
                var orderLines = getPOLineNo(poIds,results);
                return [results,orderLines];
                
            }catch(e){
                log.error('getPOLines','Error - ' + e.message);
                return null;
            }
        }
        
        function getPOLineNo(poIds,results){
        	try{
        		var soSearch = search.load({
                	id: 'customsearchct_results_so' });
            	
                var defaultFilters = soSearch.filters;
                var customFilters = [					
                     search.createFilter({
					     name : 'internalid',
					     join: 'applyingtransaction',
					     operator : search.Operator.ANYOF,
					     values : poIds
                     })
				];
                defaultFilters = defaultFilters.concat(customFilters);
                soSearch.filters = defaultFilters;
        		/*var filters = [
					search.createFilter({
					     name : 'internalid',
					     join: 'applyingtransaction',
					     operator : search.Operator.ANYOF,
					     values : poIds
					}),
					search.createFilter({
					     name : 'mainline',
					     operator : search.Operator.IS,
					     values : false
					}),
					search.createFilter({
					     name : 'type',
					     join : 'item',
					     operator : search.Operator.NONEOF,
					     values : 'subtotal'
					})
        		];

        		var columns = [
	               search.createColumn({
	             	  name: 'custcol_order_line_no'
	               }),
	               search.createColumn({
		              name: 'custcol_scm_customerpartnumber'
		           }),
		           search.createColumn({
			          name: 'rate'
			       }),
			       search.createColumn({
		              name: 'trandate',
		              join: 'purchaseorder',
		        	  sort: search.Sort.ASC
			       }),
			       search.createColumn({
			              name: 'purchaseorder',
			              sort: search.Sort.ASC
				   })
        		];*/
        		
        		 var newSearch = searchScript.init(null,'customsearchct_results_so'); //soSearch.filters
                 var searchResults = newSearch.getResults();
//                 log.debug('getPOLineNo','Results Length - ' + JSON.stringify(searchResults)); 
                 log.debug('getPOLineNo','Results - ' + JSON.stringify(searchResults)); 
                 return searchResults;
        		
        	}catch(e){
        		log.error('getPOLineNo','Error - ' + e.message);
        	}
        }
        
		/******************************************************************
		 * Populates the form with the data from the
		 * 'Consolidated Transaction Results - DO NOT DELETE' saved search
		 *
		 * @param sublist
		 * @param results
		 ******************************************************************/
        function populateSublist(sublist,groupedResults,selectedLines,changedQtys){
            try{

            	//log.debug('populateSublist','Selected Lines - ' + selectedLines);
            	//log.debug('populateSublist','Changed Qtys - ' + changedQtys);
            	if(hasValue(selectedLines)){
            		selectedLines = JSON.parse(selectedLines);
            	}
            	if(hasValue(changedQtys)){
            		changedQtys = JSON.parse(changedQtys);
            	}
            	
            	var i = 0;
            	var results = groupedResults[i];
            	var poNums = groupedResults[1];
//            	log.debug('populateSublist','PO Arr - ' + JSON.stringify(poNums));
            	for(i; i < results.length; i++){
            		var customer = results[i].getValue({ name: 'entity',join: 'createdFrom'});
            		var soNum = results[i].getValue('createdfrom');

            		if(!hasValue(customer)){
            			customer = results[i].getValue({ name: 'entity',join: 'custcol_original_so'});
            			soNum = results[i].getValue('custcol_original_so');
            		}

//                    sublist.setSublistValue({id:'custpage_po_cust', line:i, value:isEmpty(results[i].getValue({ name: 'entity',join: 'createdFrom'}))});
            		sublist.setSublistValue({id:'custpage_po_cust', line:i, value:isEmpty(customer)});
                    sublist.setSublistValue({id:'custpage_po_vendor', line:i, value:isEmpty(results[i].getValue('mainname'))});
                    sublist.setSublistValue({id:'custpage_po_date', line:i, value:isEmpty(results[i].getValue('trandate'))});
                    sublist.setSublistValue({id:'custpage_ship_date', line:i, value:isEmpty(results[i].getValue('custcol_confirmed_ship_date'))});
                    sublist.setSublistValue({id:'custpage_po_number', line:i, value:isEmpty(results[i].id)});
                    sublist.setSublistValue({id:'custpage_so_num', line:i, value:isEmpty(soNum)});
                    sublist.setSublistValue({id:'custpage_item', line:i, value:isEmpty(results[i].getValue('item'))});
                    var origQty = results[i].getValue('quantity');
                    var qtyReceived = results[i].getValue('quantityshiprecv');
                    var newQty = results[i].getValue('formulanumeric')//origQty - qtyReceived; 
                    sublist.setSublistValue({id:'custpage_qty', line:i, value:isEmpty(newQty)});
                    sublist.setSublistValue({id:'custpage_orig_qty', line:i, value:isEmpty(origQty)});
                    sublist.setSublistValue({id:'custpage_max_qty', line:i, value:isEmpty(newQty)});  
                    sublist.setSublistValue({id:'custpage_qty_received', line:i, value:isEmpty(qtyReceived)});
                   // sublist.setSublistValue({id:'custpage_rate', line:i, value:isEmpty(results[i].getValue('rate'))});
                    sublist.setSublistValue({id:'custpage_rate', line:i, value:isEmpty(results[i].getValue('custcol_so_item_rate'))});
                    sublist.setSublistValue({id:'custpage_po_lineid', line:i, value:isEmpty(results[i].getValue('line'))});
                    sublist.setSublistValue({id:'custpage_order_reference', line:i, value:isEmpty(results[i].getText({name: 'custbody_order_reference',join: 'createdFrom'}))});
//                    sublist.setSublistValue({id:'custpage_po_line_num', line:i, value:isEmpty(poNums[i].getValue('custcol_order_line_no'))});
//                    sublist.setSublistValue({id:'custpage_cust_pn', line:i, value:isEmpty(poNums[i].getValue('custcol_scm_customerpartnumber'))}); 
                    sublist.setSublistValue({id:'custpage_po_line_num', line:i, value:isEmpty(results[i].getValue('custcol_order_line_no'))});
                    sublist.setSublistValue({id:'custpage_cust_pn', line:i, value:isEmpty(results[i].getValue('custcol_cpn'))});
                    sublist.setSublistValue({id:'custpage_so_line_unique_key', line:i, value:isEmpty(results[i].getValue('custcol_so_line_unique_key'))});
                   // log.audit('populateSublist','Cust PN - ' + isEmpty(poNums[i].getValue('custcol_scm_customerpartnumber')));
//                    log.debug('populateSublist','Order Line - ' + poNums[i].getValue('custcol_order_line_no'));
                    //custcol_so_line_unique_key
                    var lineKey = results[i].getValue('lineuniquekey');
                    sublist.setSublistValue({id:'custpage_po_linekey', line:i, value:isEmpty(lineKey)});
                    if(selectedLines != undefined){
	                    for(var k = 0; k < selectedLines.length; k++){
	                		if(lineKey == selectedLines[k].lineKey){
	                			sublist.setSublistValue({id:'custpage_line_selected', line:i, value:'T'});
	                			//sublist.setSublistValue({id:'custpage_qty', line:i, value:selectedLines[k].qty});
	                			break;
	                		}
	                	}
                    }
                    
                    if(changedQtys != undefined){
                    	for(var j = 0; j < changedQtys.length; j++){
	                		if(lineKey == changedQtys[j].lineKey){
	                			sublist.setSublistValue({id:'custpage_qty', line:i, value:changedQtys[j].qty});
	                			break;
	                		}
	                		
	                	}
                    }
                    
            	}

            }catch(e){
                log.error('populateSublist','Error - ' + e.message);
            }
        }
        
		/******************************************************************
		 * Gets the lines selected by the user, calls the function that 
		 * creates the Consolidated Transaction records and then calls the
		 * Map/Reduce script that will create the Item Receipts
		 * and Fulfillments
		 *
		 * @param context
		 * @param preSelectedLines
		 ******************************************************************/
        function getSelectedLines(context,preSelectedLines){
        	try{
        		var request = context.request;
        		
        		var lineCnt = request.getLineCount('custpage_po_lines');
            	log.debug('getSelectedLines','Line Count - ' + lineCnt);
            	
            	var selectedLines = [];
            	for(var i = 0; i <= lineCnt; i++){
            		try{
            			var obj = {};
	            		var addLine = request.getSublistValue('custpage_po_lines', 'custpage_line_selected', i);
//	            		log.debug('getSelectedLines','Add Line - ' + addLine);
	            		
            			if(addLine == 'T'){
            				obj.customer = request.getSublistValue('custpage_po_lines', 'custpage_po_cust', i);
            				obj.vendor = request.getSublistValue('custpage_po_lines', 'custpage_po_vendor', i);
            				obj.item = request.getSublistValue('custpage_po_lines', 'custpage_item', i);
            				obj.qty = request.getSublistValue('custpage_po_lines', 'custpage_qty', i);
            				obj.rate = request.getSublistValue('custpage_po_lines', 'custpage_rate', i);
            				obj.soId = request.getSublistValue('custpage_po_lines', 'custpage_so_num', i);
            				obj.po = request.getSublistValue('custpage_po_lines', 'custpage_po_number', i);
            				obj.orderRefNum = request.getSublistValue('custpage_po_lines', 'custpage_order_reference', i);
            				obj.lineKey = request.getSublistValue('custpage_po_lines', 'custpage_po_linekey', i);
            				obj.lineId = request.getSublistValue('custpage_po_lines', 'custpage_po_lineid', i);
            				obj.poLineNum = request.getSublistValue('custpage_po_lines', 'custpage_po_line_num', i);
            				obj.custPN = request.getSublistValue('custpage_po_lines','custpage_cust_pn', i);
            				obj.soLineKey = request.getSublistValue('custpage_po_lines','custpage_so_line_unique_key', i);
            				selectedLines.push(obj);
            			}
            		}catch(e){
            			log.error('getSelectedLines', 'error loop - ' + e.message);
            		}
            	}

            	log.debug('getSelectedLines','Selected Lines - ' + JSON.stringify(selectedLines));
            	/*
            	 * If there is a value for the preSelectedLines var turn it into a JSON object, 
            	 * concat the array that holds the lines from the UI and the previous lines chosen
            	 * not shown in the UI and remove duplicate values
            	 */
            	if(hasValue(preSelectedLines)){
	            	var preSelectedLines = JSON.parse(preSelectedLines);
	            	log.debug('getSelectedLines','Pre-Selected Lines - ' + preSelectedLines);
	            	for(var m = 0; m < selectedLines.length; m++){
	            		  var lineKey = selectedLines[m].lineKey;
	            		  for(var k = 0; k < preSelectedLines.length;k++){
//	            			  log.debug('getSelectedLines','Preselected Line Key - ' + preSelectedLines[k].lineKey);
	            				if(preSelectedLines[k].lineKey == lineKey){
//	            					log.debug('getSelectedLines','Removing lines');
	            					preSelectedLines.splice(k, 1);
	            				}
	            		  }
	            	}
//	            	log.debug('getSelectedLines','NEW Pre-Selected Lines - ' + preSelectedLines);
	            	selectedLines = selectedLines.concat(preSelectedLines);
	            	selectedLines = checkArray(selectedLines);
	            	log.debug('getSelectedLines','NEW Selected Lines - ' + JSON.stringify(selectedLines));
            	}
            	LINE_LENGTH = selectedLines.length; 
//            	var lines = groupLines(selectedLines);
            	var lines = groupBy(selectedLines, function(record)
            			{
            		  return [record.customer, record.vendor];
            		});
            	log.debug('getSelectedLines','Grouped Data - ' + JSON.stringify(lines));

            	var successfulTransactions = [];
            	for(var j = 0; j < lines.length; j++){
        			log.debug('getSelectedLines','Transaction Data - ' + JSON.stringify(lines[j]));
        			var transId = createConsolidatedTransaction(lines[j]);
        			log.debug('getSelectedLines','Transaction Id - ' + transId);
        			if(transId != null){
        				successfulTransactions.push(transId);
        			}
            	}
            	
            	log.debug('getSelectedLines','Trans Created - ' + successfulTransactions);
            	
            	// Call Map/Reduce script that will create receipts and fulfillments
            	if(successfulTransactions != ''){
            		//log.debug('map','Calling Map/Reduce Script');
            		var mrTask = task.create({taskType: task.TaskType.MAP_REDUCE});
            		mrTask.scriptId = 'customscript_create_receipts_fulfillment';
            		mrTask.params = {'custscript_consolidated_ids': successfulTransactions};
            		var mrTaskId = mrTask.submit();
            	}
        	}catch(e){
        		log.error('getSelectedLines','Error - ' + e.message);
        	}
        }
        
        /**
         * Removes duplicates from the array sent in
         * 
         * @param data
         */
        function checkArray(data){
        	var obj = {};

        	for ( var i=0, len=data.length; i < len; i++ )
        	    obj[data[i].lineKey] = data[i];

        	data = new Array();
        	for ( var key in obj )
        	    data.push(obj[key]);
        	    
        	 /* alert(JSON.stringify(data)); */
        	return data;
        }
        
        function groupBy( array , f )
        {
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        }
        
        /**
         * Groups the lines chosen by Customer
         * 
         * @param groupLines
         */
        function groupLines(selectedLines){
        	try{
        		var prevCust = '';
        		var data = [];
        		var lineArr = [];
        		for(var i = 0; i < selectedLines.length; i++){
        			var customer = selectedLines[i].customer;
        			
        			if(customer != prevCust){
        				if(i == 0 && i != selectedLines.length - 1){ 
    		  				var nextCust = selectedLines[i + 1].customer;
	    		  			if(nextCust != customer){          	    		  				
	    		  				lineArr.push(selectedLines[i]);
	    		  				data.push(lineArr);
	    		  				lineArr = [];
    	    		  			continue;
	    		  			}
    		  			}else{ //if(i != selectedLines.length - 1){
    		  				data.push(lineArr);
    		  				lineArr = [];	
    		  			}
    		  		}
        			
        			lineArr.push(selectedLines[i]);

        			// If the last row, push object into data array
	        		if(i == selectedLines.length - 1){
	        			data.push(lineArr);
	        		}
        			
        			prevCust = customer;
        			}
//        		}
        		
        		log.debug('groupLines','Final Data - ' + JSON.stringify(data));
        		
        		return data;
        	}catch(e){
        		log.error('groupLines','Error - ' + e.message);
        	}
        }
        
        /**
         * Creates the Consolidated Transaction for the data set sent in
         * 
         * @param lines
         */
        function createConsolidatedTransaction(lines){
        	try{
        		
        		var transaction = record.create({
        			type: 'customrecord_consolidated_transaction',
        			isDynamic: true
        		});
        		var customer = lines[0].customer;
        		var vendor = lines[0].vendor;
        		transaction.setValue('custrecord_ct_customer',customer);
        		var num = uniqueNum.init('customrecord_unique_id', '1', 'custrecord_unique_number');
                var uniqueNumber = num.getNextNumber();
        		transaction.setValue('name','Consolidated Transaction - ' + uniqueNumber);
        		transaction.setValue('custrecord_ct_date',new Date());
        		transaction.setValue('custrecord_ct_status','1');
        		transaction.setValue('custrecord_ct_id',uniqueNumber);
        		transaction.setValue('custrecord_ct_vendor',vendor);

        		var headerId = transaction.save();
        		
        		if(headerId == ''){
        			return;
        		}

//        		{"customer":"36","item":"10","qty":"100","rate":".50","soId":"264","po":"4","lineKey":"441"}
        		var successfulLineItems = [];
        		for(var i = 0; i < lines.length; i++){
        			try{
            			var rec = record.create({
                			type: 'customrecord_ct_line_item',
                			isDynamic: true
                		});
            			
						// C3MM Try this manual route if the format module doesn't work
						//var amount = (parseFloat(lines[i].rate) * parseFloat(lines[i].qty)).toFixed(3);
						//
						//// Must round the way NetSuite does
						//var idx = amount.indexOf('.');
						//if(idx != -1) {
						//	var decimals = amount.substr(idx + 1);
						//	
						//	if(decimals.length > 2) {
						//		amount = amount.substr(0, idx) + '.' + decimals[0];
						//		
						//		if(decimals[2] * 1 < 5)
						//			amount += decimals[1];
						//		else
						//			amount += (decimals[1] * 1) + 1;
						//	}
						//}
						
						var amountStr = format.format({ type: format.Type.CURRENCY, value: (parseFloat(lines[i].rate) * parseFloat(lines[i].qty)) });
						var amount = format.parse({ type: format.Type.CURRENCY, value: amountStr });
						
            			var po = lines[i].po;
            			var item = lines[i].item
            			rec.setValue('custrecord_ct_item',item);
            			rec.setValue('custrecord_ct_qty',lines[i].qty)
            			rec.setValue('custrecord_ct_rate',lines[i].rate);
            			rec.setValue('custrecord_ct_amount',amount);
            			rec.setValue('custrecord_ct_so_id',lines[i].soId);
            			rec.setValue('custrecord_ct_po_id',po);
            			rec.setValue('custrecord_ct_line_unique_key',lines[i].lineKey);
            			rec.setValue('custrecord_ct_header',headerId);
            			rec.setValue('custrecord_ct_line_id',lines[i].lineId);
            			rec.setValue('custrecord_ct_line_cust_ord_num',lines[i].orderRefNum);
            			rec.setValue('custrecord_ct_line_poline',lines[i].poLineNum);
            			rec.setValue('custrecord_ct_line_part_no',lines[i].custPN);
            			rec.setValue('custrecord_so_line_unique_key',lines[i].soLineKey);
            			var lineId = rec.save();
            			
            			if(hasValue(lineId)){
            				successfulLineItems.push({'lineKey':lines[i].lineKey,'poId':po});
            			}
        			}catch(e){
        				log.error('createConsolidatedTransaction','Error in line item loop - ' + e.message);
        				var obj = {};
        				obj.poId = po;
        				obj.item = item;
        				obj.customer = customer;
        				obj.error = e.message;
        				ERROR_DATA.push(obj);
        			}

        		}


        		return headerId;
        	}catch(e){
        		log.error('createConsolidatedTransaction','Error - ' + e.message);
        		var obj = {};
				obj.poId = po;
				obj.item = item;
				obj.customer = customer;
				obj.error = e.message;
				ERROR_DATA.push(obj);
        		return null;
        	}
        }
        
        /**
         * Builds the form that informs the user if there were any errors during the consolidation.
         * 
         * @param context
         */
        function buildResponseForm(context){
        	try{
        		var form = ui.createForm({
                    title: 'Consolidated Transaction Results'
                });//ERROR_DATA
        		form.clientScriptModulePath = 'SuiteScripts/Cumula 3/ConsolidatedTransactionView-Client.js';
                var message = form.addField({
                    id: 'custpage_message',
                    label: 'Consolidated Results',
                    type: ui.FieldType.TEXT,
                    source: 'item'
                }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE })
                
                if(ERROR_DATA.length == 0){
                	message.defaultValue = 'The lines were successfully consolidated.';
                }else if(LINE_LENGTH == ERROR_DATA.length){
                	message.defaultValue = 'The lines were not consolidated, please see script error log, and/or contact you administrator for support.';
                }else{
                	message.defaultValue = 'The lines were consolidated with some errors, please see script error log, and/or contact you administrator for support';
                }
                
                form.addButton({id : 'refresh_page', label : 'OK', functionName: 'refreshPage()'});
                context.response.writePage(form);
        		
        	}catch(e){
        		log.error('buildResponseForm','Error - ' + e.message);
        	}
        	
        }
        
        /**
         * Groups the data by PO's so their consolidated lines parent record
         * can be added to the PO lines.
         * 
         * @param arr
         * @param ctId
         */
        function groupByTransactions(arr,ctId){
        	var prevPO = '';
    		var data = [];
    		var poArr = [];
    		for(var i = 0; i < arr.length; i++){
            	var po = arr[i].poId;
            	log.debug('groupByTransactions','Po - ' + po);
            	
            	if(po != prevPO && prevPO != ''){
            		//addTransactionToPO(poArr,prevPO,ctId);
            		poArr = [];
            	}

            	poArr.push(arr[i].lineKey);
            	
            	if(i == arr.length - 1){
            		//addTransactionToPO(poArr,po,ctId);
            	}
            	prevPO = po;
    		}
        }
        
        /**
         * Adds the Consolidated Transaction to the Purchase Order lines that were
         * consolidated
         * 
         * @param successfulLineItems
         * @param poId
         * @param ctId
         */
        function addTransactionToPO(successfulLineItems,poId,ctId){
        	log.debug('addTransactionToPO','Array - ' + successfulLineItems + ' PO Id - ' + poId);

        	var po = record.load({
        		type: record.Type.PURCHASE_ORDER,
        		id: poId
        	})

        	for(var i = 0; i < po.getLineCount('item'); i++){
        		var lineKey = po.getSublistValue('item','lineuniquekey',i);
        		log.debug('addTransactionToPO','PO Line Key - ' + lineKey);
        		for(var k = 0; k < successfulLineItems.length; k++){
        			if(successfulLineItems[k] == lineKey){
        				po.setSublistValue('item','custcol_consolidated_transaction',i,ctId);
        			}
        		}
        	}
        	
        	po.save();
        }

		/******************************************************************
		 * If the value sent in is an empty string or undefined return a 
		 * null value. 
		 *
		 * @param value
		 ******************************************************************/
        function isEmpty(value){
            if(value == "" || value == undefined){
                return null
            }
            
            return value;
        }
        
		/******************************************************************
		 * If the value sent in is not null or an empty string, return true. 
		 *
		 * @param value
		 ******************************************************************/
    	function hasValue(value){
    		if(value != null && value != '' && value != undefined){
    			return true;
    		}
    		
    		return false;
    	}
        
        return {
            onRequest: run,
            
        };
    });