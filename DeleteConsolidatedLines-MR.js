/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 * @NScriptType MapReduceScript
 * 
 */
define(['N/search', 'N/record', 'SuiteScripts/Cumula 3/Search-20', 'N/runtime','/SuiteScripts/Cumula 3/SearchForConsolidateLines', 'N/file'],
    function(search, record, searchScript, runtime, lineItems, file)
    { 	

        function getInputData()
        {
            try{
           
            	var scriptObj = runtime.getCurrentScript();
            	var ctId = scriptObj.getParameter({name: 'custscript_consolidated_transaction'});
                log.debug('getInputData','Ct id - ' + ctId);      
                
                var searchResults = lineItems.SearchLines(ctId);
                var records = getRelatedTransactions(ctId);
                log.debug('getInputData','Records - ' + JSON.stringify(records));
                searchResults = searchResults.concat(records);
                
                log.debug('getInputData','Final Array - ' + JSON.stringify(searchResults));
                return searchResults;
            }catch(e){
                log.error('getInputData','Error - ' + e.message);
            }

        }

        function map(context)
        {
        	try{
                var searchResult = JSON.parse(context.value);
                log.debug('map','Search Result - ' + JSON.stringify(searchResult));
                //log.debug('map','Search Result - ' + searchResult);

                var recId = searchResult.id;
                var recType = searchResult.recordType

                log.debug('map','Rec Id - ' + recId + ' Rec Type - ' + recType);

                record.delete({
                	type: recType,
                	id: recId
                });

        	}catch(e){
        		log.error('map','Error - ' + e.message);
        	}
        }
        
        function getRelatedTransactions(ctId){
        	var filters = [
               search.createFilter({
            	   name: 'custbody_consolidated_trans',
            	   operator : search.Operator.ANYOF,
            	   values: ctId
               })
           ];
        	/*

            */
        	var newSearch = searchScript.init('transaction',null,filters);
            var searchResults = newSearch.getResults();
            log.debug('getRelatedTransactions','Results - ' + JSON.stringify(searchResults));
            var recIds = [];
            var prevRecId = '';
            for(var i = 0; i < searchResults.length; i++){
            	var recType = searchResults[i].recordType;
            	if(recType == 'purchaseorder'){
            		prevRecId = recId;
            		continue;
            	}
            	
            	var recId = searchResults[i].id;
            	if(recId != prevRecId){
            		recIds.push(searchResults[i]);
            	}
            	
            	prevRecId = recId;
            	
            }
            
            return recIds;
        }
        
        function summarize(){
        	var scriptObj = runtime.getCurrentScript();
        	var ctId = scriptObj.getParameter({name: 'custscript_consolidated_transaction'});
        	
        	var ct = record.load({
        		type: 'customrecord_consolidated_transaction',
        		id: ctId
        	});
        	
        	var pdfs = ['custrecord_ct_pdf','custrecord_proforma_inv','custrecord_shipping_notice','custrecord_contract_to_sell'];
        	
        	for(var i = 0; i < pdfs.length; i++){
        		var pdf = ct.getValue(pdfs[i]);
        		
        		if(!isNull(pdf)){
        			file.delete({ id: pdf
        			});
        		}
        	}
        	
        	record.delete({
             	type: 'customrecord_consolidated_transaction',
             	id: ctId
            });
        }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
      
       
        return {
            getInputData: getInputData,
            map: map,
            //reduce: reduce,
            summarize: summarize
        };
    });