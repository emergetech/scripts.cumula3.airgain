/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * Copies certain native and custom fields on the Sales Order to custom
 * fields created to be moved over to a Purchase Order when it is created
 * from a Sales Order.
 */

define(['N/record'],
    function (record)
    {
        function onAfterSubmit(context)
        {
        	try{   
	        	var rec = context.newRecord;
	        	var id = rec.getValue('id');
	        	
	        	var transaction = record.load({
	        		type: record.Type.SALES_ORDER,
	        		id:id
	        	})
	        	
	        	for(var i = 0; i < transaction.getLineCount('item'); i++){
	        		var soLineKey = transaction.getSublistValue('item','lineuniquekey',i);
	        		var rate = transaction.getSublistValue('item','rate',i);
	        		var cpn = transaction.getSublistValue('item','custcol_scm_customerpartnumber',i);
	        		
	        		transaction.setSublistValue('item','custcol_so_line_unique_key',i,soLineKey);
	        		transaction.setSublistValue('item','custcol_so_item_rate',i,rate);
	        		transaction.setSublistValue('item','custcol_cpn',i,cpn);
	        	}
	        	
	        	transaction.save();

	        	
        	}catch(e){
        		log.error('onAfterSubmit','Error - ' + e.message);
        	}
           
        }
        
        function isNull(value){
        	if(value == null || value == '' || value == undefined){
        		return true;
        	}
        	
        	return false;
        }
        
        return {
        	afterSubmit: onAfterSubmit
        };
    });

